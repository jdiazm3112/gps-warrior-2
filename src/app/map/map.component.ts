import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet'

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  private map:any 

  constructor() { }

  ngOnInit(): void {
    this.initMap()
  }

  initMap() {
    this.map = L.map('map', {
      center: [41.38442, 2.17516],
      zoom: 13
    })

    const tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1Ijoiam9uYXRoYW5kaWF6IiwiYSI6ImNrYnNsem04ajAxOXIycHVseGdnajJ2bWYifQ.031mg0WsmXLzVruWgpQ0tw'
    })

    tiles.addTo(this.map)

    // create marker 

    const icon = L.icon({
      iconUrl: 'assets/marker.png',
      iconSize: [50, 50]
    })


    L.marker([41.38442, 2.17516], { icon }).addTo(this.map)


    const myLines = [{
        "type": "LineString",
        "coordinates": [
          [2.16548,41.42029],
          [2.16555,41.42033],
          [2.16564,41.42035],
          [2.16571,41.42036],
          [2.16575,41.4204],
          [2.16583,41.42038],
          [2.1659,41.42038],
          [2.16597,41.42041],
          [2.16605,41.42042],
          [2.16614,41.42042],
          [2.1662,41.42043],
          [2.16627,41.42042],
          [2.16635,41.42041],
          [2.16642,41.42039],
          [2.16648,41.42039],
          [2.16668,41.42038],
          [2.16675,41.4204],
          [2.16698,41.42041],
          [2.16704,41.42044],
          [2.16706,41.42049],
          [2.16708,41.42056],
          [2.16711,41.42061],
          [2.16717,41.42062],
          [2.16725,41.42059],
          [2.1673,41.42055],
          [2.16732,41.4205],
          [2.16733,41.42044],
          [2.16731,41.42039],
          [2.16718,41.42023],
          [2.16709,41.42014],
          [2.167,41.42007],
          [2.16681,41.41989],
          [2.16677,41.41985],
          [2.16675,41.4198],
          [2.16685,41.41985]
        ]
    }];
    
    var myStyle = {
        "color": "#0087ff",
        "weight": 5,
        "opacity": 0.65
    };
    
    L.geoJSON(myLines, {
      style: myStyle
    }).addTo(this.map)
  }
}
